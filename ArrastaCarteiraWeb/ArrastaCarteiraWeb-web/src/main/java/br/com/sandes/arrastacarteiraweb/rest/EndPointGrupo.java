/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.sandes.arrastacarteiraweb.rest;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import org.igniterealtime.restclient.RestApiClient;
import org.igniterealtime.restclient.entity.AuthenticationToken;
import org.igniterealtime.restclient.entity.MUCRoomEntity;
import org.jboss.resteasy.client.jaxrs.BasicAuthentication;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.GrupoDto;
import br.com.sandes.arrastacarteiraweb.dto.UserXMPP;
import br.com.sandes.arrastacarteiraweb.dto.UsuarioDto;
import br.com.sandes.arrastacarteiraweb.interfaces.CursoService;
import br.com.sandes.arrastacarteiraweb.interfaces.GrupoService;
import br.com.sandes.arrastacarteiraweb.interfaces.UsuarioService;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the
 * members table.
 */
@Path("/grupo")
@RequestScoped
public class EndPointGrupo {
	@Inject
	private Logger log;
	@Inject
	private GrupoService grupoService;
	@Inject
	private CursoService cursoService;
	@Inject
	private UsuarioService usuarioService;

	@GET
	@Path("validaConexao")
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean testeConexao() {
		log.info("acesso ao teste");
		try {
			this.criaGrupoXmppManual(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Boolean.TRUE;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<GrupoDto> getTodos() {

		return grupoService.getList().stream().map(entity -> this.grupoService.toDto(entity))
				.collect(Collectors.toList());

	}

	@POST
	@Path("/localizaPorCurso")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<GrupoDto> localizaPorCurso(CursoDto cursoDto) {
		return grupoService.localizaPorCurso(cursoService.fromDto(cursoDto)).stream()
				.map(entity -> grupoService.toDto(entity)).collect(Collectors.toList());

	}

	@POST
	@Path("/localizaGruposPrivados")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<GrupoDto> localizaGruposPrivados(UsuarioDto usuarioDto) {
		return grupoService.localizaGruposPrivados(usuarioService.fromDto(usuarioDto)).stream()
				.map(entity -> grupoService.toDto(entity)).collect(Collectors.toList());
	}

	@POST
	@Path("/criaGrupo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GrupoDto criaGrupo(GrupoDto grupoDtoParametro) throws Exception {

		GrupoDto grupoDto = grupoService.toDto(grupoService.criaGrupo(grupoService.fromDto(grupoDtoParametro)));
		criaGrupoXmppManual(grupoDto); 
		for (UsuarioDto usuarioDto : grupoDto.getListaUsuarios()) {
			this.insereUsuarioGrupoPrivado(usuarioDto, grupoDto);
		}
		return grupoDto;

	}

	@PUT
	@Path("/atualizaGrupo")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GrupoDto atualizaGrupo(GrupoDto grupoDto) throws Exception {

		return grupoService.toDto(grupoService.atualizar(grupoService.fromDto(grupoDto)));

	}


	private Boolean insereUsuarioGrupoPrivado(UsuarioDto usuarioDto, GrupoDto grupoDto) throws Exception {
		int index = usuarioDto.getEmail().indexOf('@');
		final String username = usuarioDto.getEmail().substring(0, index);

		UserXMPP user = new UserXMPP();
		user.setUsername(username);
		user.setPassword(usuarioDto.getSenha());

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://35.163.32.23:9090/plugins/restapi/v1/chatrooms/"
				+ grupoDto.getId().toString() + "/members/" + username);
		target.register(new BasicAuthentication("admin", "admin"));

		Response response = target.request().post(null);
		response.close();

		if (response.getStatusInfo().getFamily() != Family.SUCCESSFUL) {
			throw new Exception("erro XMPP");
		}

		return Boolean.TRUE;

	}
	
	private Boolean criaGrupoXmppManual(GrupoDto grupoDto) throws Exception {
	
		MUCRoomEntity chatRoom = new MUCRoomEntity(grupoDto.getId().toString(), grupoDto.getNome(), grupoDto.getNome());
		chatRoom.setMaxUsers(50);
		chatRoom.setPersistent(Boolean.TRUE);
		chatRoom.setCanAnyoneDiscoverJID(Boolean.TRUE);
		
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://35.163.32.23:9090/plugins/restapi/v1/chatrooms");
		target.register(new BasicAuthentication("admin", "admin"));

		Response response = target.request().post(Entity.entity(chatRoom, MediaType.APPLICATION_JSON));
		response.close();

		if (response.getStatusInfo().getFamily() != Family.SUCCESSFUL) {
			throw new Exception("erro XMPP");
		}

		return Boolean.TRUE;

	}
	

}
