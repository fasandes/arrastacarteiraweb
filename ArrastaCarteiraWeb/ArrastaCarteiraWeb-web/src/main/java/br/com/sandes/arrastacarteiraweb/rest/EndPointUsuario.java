/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.sandes.arrastacarteiraweb.rest;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import org.jboss.resteasy.client.jaxrs.BasicAuthentication;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.GrupoDto;
import br.com.sandes.arrastacarteiraweb.dto.UserXMPP;
import br.com.sandes.arrastacarteiraweb.dto.UsuarioDto;
import br.com.sandes.arrastacarteiraweb.interfaces.GrupoService;
import br.com.sandes.arrastacarteiraweb.interfaces.UsuarioService;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the
 * members table.
 */
@Path("/usuario")
@RequestScoped
public class EndPointUsuario {
	@Inject
	private Logger log;
	@Inject
	private UsuarioService usuarioService;
	@Inject
	private GrupoService grupoService;

	@GET
	@Path("/validaConexao")
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean testeConexao() {
		log.info("acesso ao teste");
		return Boolean.TRUE;
	}

	@GET
	@Path("/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public UsuarioDto localizaPorEmail(@PathParam("email") String email) {

		return usuarioService.toDto(usuarioService.localizarPorEmail(email));

	}
	
	@POST
	@Path("/localizaPorCurso")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<UsuarioDto> localizaPorCurso(CursoDto cursoDto) {
		return usuarioService.localizaPorCurso(cursoDto).stream().map(entity -> usuarioService.toDto(entity)).collect(Collectors.toList());
		
	}

	@POST
	@Path("/cadastraUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsuarioDto cadastraUsuario(UsuarioDto dto) {

		if (cadastraUsuarioXMPP(dto)) {
			return usuarioService.toDto(usuarioService.salvar(usuarioService.fromDto(dto)));
		} else {
			Response.serverError();
			return null;
		}

	}

	@PUT
	@Path("/atualizar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public UsuarioDto atualizar(UsuarioDto dto) {
		if (insereUsuarioGrupos(dto)) {
			return usuarioService.toDto(usuarioService.atualizar(usuarioService.fromDto(dto)));
		} else {
			Response.serverError();
			return null;
		}

	}

	private boolean cadastraUsuarioXMPP(UsuarioDto usuarioDto) {
		int index = usuarioDto.getEmail().indexOf('@');
		final String username = usuarioDto.getEmail().substring(0, index);

		UserXMPP user = new UserXMPP();
		user.setUsername(username);
		user.setPassword(usuarioDto.getSenha());

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://35.163.32.23:9090/plugins/restapi/v1/users");
		target.register(new BasicAuthentication("admin", "admin"));
		Response response = target.request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
		response.close();

		return response.getStatusInfo().getFamily() == Family.SUCCESSFUL;
	}

	private boolean insereUsuarioGrupos(UsuarioDto usuarioDto) {

		int index = usuarioDto.getEmail().indexOf('@');
		final String username = usuarioDto.getEmail().substring(0, index);

		UserXMPP user = new UserXMPP();
		user.setUsername(username);
		user.setPassword(usuarioDto.getSenha());

		Client client = ClientBuilder.newClient();

		List<GrupoDto> lista = grupoService.localizaPorCurso(usuarioDto.getCursoDto()).stream()
				.map(entity -> grupoService.toDto(entity)).collect(Collectors.toList());

		for (GrupoDto grupoDto : lista) {

			WebTarget target = client.target(
					"http://35.163.32.23:9090/plugins/restapi/v1/chatrooms/" + grupoDto.getId().toString() + "/members/" + username);
			target.register(new BasicAuthentication("admin", "admin"));
			
			Response response = target.request().post(null);
			response.close();

			if (response.getStatusInfo().getFamily() != Family.SUCCESSFUL) {
				return Boolean.FALSE;
			}
		}

		return Boolean.TRUE;
	}

}
