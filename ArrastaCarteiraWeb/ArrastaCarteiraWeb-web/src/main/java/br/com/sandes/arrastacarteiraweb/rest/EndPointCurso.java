/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.sandes.arrastacarteiraweb.rest;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.InstituicaoDto;
import br.com.sandes.arrastacarteiraweb.interfaces.CursoService;
import br.com.sandes.arrastacarteiraweb.interfaces.InstituicaoService;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the
 * members table.
 */
@Path("/curso")
@RequestScoped
public class EndPointCurso {
	@Inject
	private Logger log;
	@Inject
	private CursoService cursoService;
	@Inject
	private InstituicaoService instituicaoService;

	@GET
	@Path("validaConexao")
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean testeConexao() {
		log.info("acesso ao teste");
		return Boolean.TRUE;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
//	public void localizaPorInstituicao(@Suspended AsyncResponse asyncResponse, InstituicaoDto instituicaoDto) {
	public List<CursoDto> localizaPorInstituicao(InstituicaoDto instituicaoDto) {
		return cursoService.localizaCursoPorInstituicao(instituicaoService.fromDto(instituicaoDto)).stream()
				.map(entity -> cursoService.toDto(entity)).collect(Collectors.toList());
		
//		final Response resp = Response.ok(cursoService.localizaCursoPorInstituicao(instituicaoService.fromDto(instituicaoDto)).stream()
//				.map(entity -> cursoService.toDto(entity)).collect(Collectors.toList())).type(MediaType.APPLICATION_JSON)
//				.build();
//		asyncResponse.resume(resp);

	}

}
