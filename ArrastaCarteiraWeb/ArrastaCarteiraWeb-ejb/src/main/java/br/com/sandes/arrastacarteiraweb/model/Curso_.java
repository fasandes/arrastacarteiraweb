package br.com.sandes.arrastacarteiraweb.model;

import java.util.List;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;



@StaticMetamodel(Curso.class)
public class Curso_ {

	public static volatile SingularAttribute<Curso,Integer> id;
	public static volatile SingularAttribute<Curso,String> nome;
	public static volatile SingularAttribute<Curso,Instituicao> instituicao;
	public static volatile SingularAttribute<Curso,List<Grupo>> grupos;

	

}