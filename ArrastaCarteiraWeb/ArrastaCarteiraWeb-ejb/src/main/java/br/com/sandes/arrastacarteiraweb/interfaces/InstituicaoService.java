package br.com.sandes.arrastacarteiraweb.interfaces;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.InstituicaoDto;
import br.com.sandes.arrastacarteiraweb.model.Instituicao;

public interface InstituicaoService extends dao<Instituicao, Integer> {

	InstituicaoDto toDto(Instituicao Instituicao);
	
	
	Instituicao fromDto(InstituicaoDto instituicaoDto);

}
