package br.com.sandes.arrastacarteiraweb.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.sandes.arrastacarteiraweb.dao.InstituicaoDao;
import br.com.sandes.arrastacarteiraweb.dto.InstituicaoDto;
import br.com.sandes.arrastacarteiraweb.interfaces.CursoService;
import br.com.sandes.arrastacarteiraweb.interfaces.InstituicaoService;
import br.com.sandes.arrastacarteiraweb.model.Instituicao;

@Stateless
public class InstituicaoServiceImpl implements InstituicaoService {

	@Inject
	private InstituicaoDao dao;

	@Inject
	private CursoService cursoService;

	@Override
	public Instituicao salvar(Instituicao entity) {
		return dao.salvar(entity);
	}

	@Override
	public Instituicao atualizar(Instituicao entity) {
		return dao.atualizar(entity);
	}

	@Override
	public void remover(Integer id) {
		dao.remover(id);
	}

	@Override
	public List<Instituicao> getList() {
		return dao.getList();
	}

	@Override
	public Instituicao encontrar(Integer id) {
		return dao.encontrar(id);
	}

	@Override
	public InstituicaoDto toDto(Instituicao instituicao) {

		if (instituicao == null) {
			return null;
		}
		final InstituicaoDto dto = new InstituicaoDto();

		dto.setId(instituicao.getId());
		dto.setNome(instituicao.getNome());
		dto.setCursos(instituicao.getCursos().stream().map(entity -> this.cursoService.toDto(entity))
				.collect(Collectors.toList()));

		return dto;
	}

	@Override
	public Instituicao fromDto(InstituicaoDto instituicaoDto) {
		final Instituicao entity = new Instituicao();
		entity.setId(instituicaoDto.getId());
		entity.setCursos(instituicaoDto.getCursos().stream().map(dto -> this.cursoService.fromDto(dto, entity))
				.collect(Collectors.toList()));

		return entity;
	}

}
