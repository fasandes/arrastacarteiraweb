package br.com.sandes.arrastacarteiraweb.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.sandes.arrastacarteiraweb.model.Curso;
import br.com.sandes.arrastacarteiraweb.model.Curso_;
import br.com.sandes.arrastacarteiraweb.model.Instituicao;

public class CursoDao extends GenericDao<Curso, Integer> {

	public CursoDao() {
		super(Curso.class);
	}

	public List<Curso> localizaCursoPorInstituicao(Instituicao instituicao) {
		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Curso> criteria = this.entityManager.getCriteriaBuilder().createQuery(Curso.class);
		Root<Curso> personRoot = criteria.from(Curso.class);
		criteria.select(personRoot);
		criteria.where(builder.equal(personRoot.get(Curso_.instituicao), instituicao));
		return this.entityManager.createQuery(criteria).getResultList();
	}

}
