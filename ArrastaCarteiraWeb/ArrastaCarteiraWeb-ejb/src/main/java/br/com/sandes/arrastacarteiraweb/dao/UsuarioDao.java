package br.com.sandes.arrastacarteiraweb.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.sandes.arrastacarteiraweb.model.Curso;
import br.com.sandes.arrastacarteiraweb.model.Grupo;
import br.com.sandes.arrastacarteiraweb.model.Usuario;
import br.com.sandes.arrastacarteiraweb.model.Usuario_;

public class UsuarioDao extends GenericDao<Usuario, Long> {

	public UsuarioDao() {
		super(Usuario.class);
	}

	public Usuario localizarPorEmail(String email) {
		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = this.entityManager.getCriteriaBuilder().createQuery(Usuario.class);
		Root<Usuario> personRoot = criteria.from(Usuario.class);
		criteria.select(personRoot);
		criteria.where(builder.equal(personRoot.get(Usuario_.email), email));
		try {
			return this.entityManager.createQuery(criteria).getSingleResult();
		} catch (NoResultException | NonUniqueResultException e) {
			return null;
		}
	}

	public List<Usuario> localizarPorCurso(Curso curso) {
		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = this.entityManager.getCriteriaBuilder().createQuery(Usuario.class);
		Root<Usuario> personRoot = criteria.from(Usuario.class);
		criteria.select(personRoot);
		criteria.where(builder.equal(personRoot.get(Usuario_.curso), curso));
		try {
			return this.entityManager.createQuery(criteria).getResultList();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
		
	}



}
