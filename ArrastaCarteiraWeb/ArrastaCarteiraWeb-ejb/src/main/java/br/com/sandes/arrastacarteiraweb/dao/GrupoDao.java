package br.com.sandes.arrastacarteiraweb.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.GrupoDto;
import br.com.sandes.arrastacarteiraweb.model.Curso;
import br.com.sandes.arrastacarteiraweb.model.Grupo;
import br.com.sandes.arrastacarteiraweb.model.Usuario;

public class GrupoDao extends GenericDao<Grupo, Integer> {

	public GrupoDao() {
		super(Grupo.class);
	}

	public List<Grupo> localizarPorCurso(Curso curso) {
		final TypedQuery<Grupo> qry = this.entityManager
				.createQuery("SELECT G FROM Grupo G INNER JOIN G.curso C WHERE C.id = ?1 AND G.privado = ?2", Grupo.class);
		qry.setParameter(1, curso.getId());
		qry.setParameter(2, Boolean.FALSE);	
		
		return  qry.getResultList();
	}
	
	public List<Grupo> localizarPorCurso(CursoDto cursoDto) {
		final TypedQuery<Grupo> qry = this.entityManager
				.createQuery("SELECT G FROM Grupo G INNER JOIN G.curso C WHERE C.id = ?1 AND G.privado = ?2", Grupo.class);
		qry.setParameter(1, cursoDto.getId());
		qry.setParameter(2, Boolean.FALSE);	
		
		return  qry.getResultList();
	}

	public Grupo criaGrupo(Grupo grupo) {
		return this.salvar(grupo);
	}

	public List<Grupo> localizaGruposPrivados(Usuario usuario) {
		final TypedQuery<Grupo> qry = this.entityManager
				.createQuery("SELECT G FROM Grupo G INNER JOIN G.listaUsuarios list WHERE list.id = ?1 AND G.privado = ?2", Grupo.class);
		qry.setParameter(1, usuario.getId());
		qry.setParameter(2, Boolean.TRUE);	
		
		return  qry.getResultList();
	}
	
}
