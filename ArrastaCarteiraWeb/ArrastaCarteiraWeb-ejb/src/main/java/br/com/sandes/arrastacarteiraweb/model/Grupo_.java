package br.com.sandes.arrastacarteiraweb.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;



@StaticMetamodel(Curso.class)
public class Grupo_ {

	public static volatile SingularAttribute<Grupo,Integer> id;
	public static volatile SingularAttribute<Grupo,String> nome;
	public static volatile SingularAttribute<Grupo,Curso> curso;

	

}