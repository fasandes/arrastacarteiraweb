package br.com.sandes.arrastacarteiraweb.dto;

import java.util.List;

public class InstituicaoDto {

	private Integer id;

	private String nome;

	private List<CursoDto> cursos;

	public InstituicaoDto() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<CursoDto> getCursos() {
		return this.cursos;
	}

	public void setCursos(List<CursoDto> cursos) {
		this.cursos = cursos;
	}

	public CursoDto addCurso(CursoDto curso) {
		getCursos().add(curso);
		curso.setInstituicao(this);

		return curso;
	}

	public CursoDto removeCurso(CursoDto curso) {
		getCursos().remove(curso);
		curso.setInstituicao(null);

		return curso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cursos == null) ? 0 : cursos.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InstituicaoDto other = (InstituicaoDto) obj;
		if (cursos == null) {
			if (other.cursos != null)
				return false;
		} else if (!cursos.equals(other.cursos))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InstituicaoDto [id=" + id + ", nome=" + nome + ", cursos=" + cursos + "]";
	}
	
	

}