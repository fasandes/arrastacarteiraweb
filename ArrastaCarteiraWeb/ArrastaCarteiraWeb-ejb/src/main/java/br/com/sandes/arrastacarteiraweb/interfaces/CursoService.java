package br.com.sandes.arrastacarteiraweb.interfaces;

import java.util.List;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.model.Curso;
import br.com.sandes.arrastacarteiraweb.model.Instituicao;

public interface CursoService extends dao<Curso, Integer> {

	CursoDto toDto(Curso Curso);
	
	Curso fromDto(CursoDto cursoDto);
	
	Curso fromDto(CursoDto cursoDto, Instituicao instituicao);

	List<Curso> localizaCursoPorInstituicao(Instituicao instituicao);

}
