package br.com.sandes.arrastacarteiraweb.interfaces;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

public interface dao<T, I extends Serializable> {

	T salvar(@Valid T entity);
	T atualizar(@Valid T entity);
	void remover(I id);

	List<T> getList();

	T encontrar(I id);

}
