package br.com.sandes.arrastacarteiraweb.model;

import java.util.List;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Instituicao.class)
public class Instituicao_ {

	public static volatile SingularAttribute<Instituicao,Integer> id;
	public static volatile SingularAttribute<Instituicao,String> nome;
	public static volatile SingularAttribute<Instituicao,List<Curso>> cursos;

}