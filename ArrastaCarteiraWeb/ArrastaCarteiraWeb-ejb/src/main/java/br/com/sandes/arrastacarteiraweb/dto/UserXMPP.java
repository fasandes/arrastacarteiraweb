package br.com.sandes.arrastacarteiraweb.dto;

public class UserXMPP {

	private String username;
	private String password;

	public UserXMPP() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
