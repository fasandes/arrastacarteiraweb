package br.com.sandes.arrastacarteiraweb.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.sandes.arrastacarteiraweb.dao.GrupoDao;
import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.GrupoDto;
import br.com.sandes.arrastacarteiraweb.interfaces.CursoService;
import br.com.sandes.arrastacarteiraweb.interfaces.GrupoService;
import br.com.sandes.arrastacarteiraweb.interfaces.TarefaService;
import br.com.sandes.arrastacarteiraweb.interfaces.UsuarioService;
import br.com.sandes.arrastacarteiraweb.model.Curso;
import br.com.sandes.arrastacarteiraweb.model.Grupo;
import br.com.sandes.arrastacarteiraweb.model.Usuario;

@Stateless
public class GrupoServiceImpl implements GrupoService {

	@Inject
	private GrupoDao dao;

	@Inject
	private CursoService cursoService;

	@Inject
	private TarefaService tarefaService;

	@Inject
	private UsuarioService usuarioService;

	@Override
	public Grupo salvar(Grupo entity) {
		return dao.salvar(entity);
	}

	@Override
	public Grupo atualizar(Grupo entity) {
		return dao.atualizar(entity);
	}

	@Override
	public void remover(Integer id) {
		dao.remover(id);

	}

	@Override
	public List<Grupo> getList() {
		return dao.getList();
	}

	@Override
	public Grupo encontrar(Integer id) {
		return dao.encontrar(id);
	}

	@Override
	public GrupoDto toDto(Grupo grupo) {

		if (grupo == null) {
			return null;
		}
		final GrupoDto dto = new GrupoDto();

		dto.setId(grupo.getId());
		dto.setNome(grupo.getNome());
		dto.setCursoDto(cursoService.toDto(grupo.getCurso()));
		dto.setPrivado(grupo.getPrivado());

		if (grupo.getListaTarefas() != null) {
			dto.setListaTarefas(grupo.getListaTarefas().stream().map(entity -> this.tarefaService.toDto(entity, dto))
					.collect(Collectors.toList()));
		}

		if (grupo.getListaUsuarios() != null) {
			dto.setListaUsuarios(grupo.getListaUsuarios().stream().map(entity -> this.usuarioService.toDto(entity))
					.collect(Collectors.toList()));
		}

		return dto;
	}

	@Override
	public Grupo fromDto(GrupoDto grupoDto) {
		final Grupo entity = new Grupo();

		entity.setId(grupoDto.getId());
		entity.setNome(grupoDto.getNome());
		entity.setCurso(cursoService.fromDto(grupoDto.getCursoDto()));
		entity.setPrivado(grupoDto.getPrivado());

		if (grupoDto.getListaTarefas() != null) {
			entity.setListaTarefas(grupoDto.getListaTarefas().stream()
					.map(dto -> this.tarefaService.fromDto(dto, entity)).collect(Collectors.toList()));
		}

		if (grupoDto.getListaUsuarios() != null) {
			entity.setListaUsuarios(grupoDto.getListaUsuarios().stream().map(dto -> this.usuarioService.fromDto(dto))
					.collect(Collectors.toList()));
		}

		return entity;
	}

	@Override
	public Grupo fromDto(GrupoDto grupoDto, Curso curso) {
		final Grupo entity = new Grupo();

		entity.setId(grupoDto.getId());
		entity.setNome(grupoDto.getNome());
		entity.setCurso(curso);
		entity.setPrivado(grupoDto.getPrivado());

		if (grupoDto.getListaTarefas() != null) {
			entity.setListaTarefas(grupoDto.getListaTarefas().stream().map(dto -> this.tarefaService.fromDto(dto))
					.collect(Collectors.toList()));
		}

		if (grupoDto.getListaUsuarios() != null) {
			entity.setListaUsuarios(grupoDto.getListaUsuarios().stream().map(dto -> this.usuarioService.fromDto(dto))
					.collect(Collectors.toList()));
		}

		return entity;
	}

	@Override
	public List<Grupo> localizaPorCurso(Curso curso) {
		return dao.localizarPorCurso(curso);
	}

	@Override
	public List<Grupo> localizaPorCurso(CursoDto cursoDto) {
		return dao.localizarPorCurso(cursoDto);
	}

	@Override
	public Grupo criaGrupo(Grupo grupo) {
		return dao.criaGrupo(grupo);
	}

	@Override
	public List<Grupo> localizaGruposPrivados(Usuario usuario) {
		return dao.localizaGruposPrivados(usuario);
	}

}
