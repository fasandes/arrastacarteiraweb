package br.com.sandes.arrastacarteiraweb.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import javax.validation.Valid;

import br.com.sandes.arrastacarteiraweb.interfaces.dao;

public abstract class GenericDao<T, I extends Serializable> implements dao<T, I> {

   @Inject
   protected EntityManager entityManager;

   private Class<T> persistedClass;

   protected GenericDao() {
   }

   protected GenericDao(Class<T> persistedClass) {
       this();
       this.persistedClass = persistedClass;
   }
   @Transactional
   public T salvar(@Valid T entity) {
   
       entityManager.persist(entity);
       entityManager.flush();
       return entity;
   }
   @Transactional
   public T atualizar(@Valid T entity) {
       entityManager.merge(entity);
       entityManager.flush();
       return entity;
   }

   public void remover(I id) {
       T entity = encontrar(id);
       EntityTransaction tx = entityManager.getTransaction();
       tx.begin();
       T mergedEntity = entityManager.merge(entity);
       entityManager.remove(mergedEntity);
       entityManager.flush();
       tx.commit();
   }

   public List<T> getList() {
       CriteriaBuilder builder = entityManager.getCriteriaBuilder();
       CriteriaQuery<T> query = builder.createQuery(persistedClass);
       query.from(persistedClass);
       return entityManager.createQuery(query).getResultList();
   }

   public T encontrar(I id) {
       return entityManager.find(persistedClass, id);
   }
}