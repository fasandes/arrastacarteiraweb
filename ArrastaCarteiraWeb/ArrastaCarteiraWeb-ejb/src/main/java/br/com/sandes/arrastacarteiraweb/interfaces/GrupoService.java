package br.com.sandes.arrastacarteiraweb.interfaces;

import java.util.List;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.GrupoDto;
import br.com.sandes.arrastacarteiraweb.model.Curso;
import br.com.sandes.arrastacarteiraweb.model.Grupo;
import br.com.sandes.arrastacarteiraweb.model.Usuario;

public interface GrupoService extends dao<Grupo, Integer> {

	GrupoDto toDto(Grupo grupo);
	
	Grupo fromDto(GrupoDto grupoDto);
	
	Grupo fromDto(GrupoDto grupoDto, Curso curso);
	
	List<Grupo> localizaPorCurso(Curso curso);
	
	List<Grupo> localizaPorCurso(CursoDto cursoDto);
	
	Grupo criaGrupo(Grupo grupo);
	
	Grupo atualizar(Grupo grupo);

	List<Grupo> localizaGruposPrivados(Usuario usuario);


}
