package br.com.sandes.arrastacarteiraweb.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.sandes.arrastacarteiraweb.dao.CursoDao;
import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.interfaces.CursoService;
import br.com.sandes.arrastacarteiraweb.interfaces.InstituicaoService;
import br.com.sandes.arrastacarteiraweb.model.Curso;
import br.com.sandes.arrastacarteiraweb.model.Instituicao;

@Stateless
public class CursoServiceImpl implements CursoService {

	@Inject
	private CursoDao dao;
	
	@Inject
	private InstituicaoService instituicaoService;

	@Override
	public Curso salvar(Curso entity) {
		return dao.salvar(entity);
	}

	@Override
	public Curso atualizar(Curso entity) {
		return dao.atualizar(entity);
	}

	@Override
	public void remover(Integer id) {
		dao.remover(id);

	}

	@Override
	public List<Curso> getList() {
		return dao.getList();
	}

	@Override
	public Curso encontrar(Integer id) {
		return dao.encontrar(id);
	}

	@Override
	public CursoDto toDto(Curso curso) {

		if (curso == null) {
			return null;
		}
		final CursoDto dto = new CursoDto();

		dto.setId(curso.getId());
		dto.setNome(curso.getNome());

		return dto;
	}

	@Override
	public Curso fromDto(CursoDto cursoDto) {
		final Curso entity = new Curso();

		entity.setId(cursoDto.getId());
		entity.setNome(cursoDto.getNome());
//		entity.setInstituicao(instituicaoService.fromDto(cursoDto.getInstituicao()));
		return entity;
	}

	@Override
	public Curso fromDto(CursoDto cursoDto, Instituicao instituicao) {
		final Curso entity = new Curso();

		entity.setId(cursoDto.getId());
		entity.setNome(cursoDto.getNome());
		entity.setInstituicao(instituicao);
		return entity;
	}

	@Override
	public List<Curso> localizaCursoPorInstituicao(Instituicao instituicao) {
		return this.dao.localizaCursoPorInstituicao(instituicao);
	}

}
