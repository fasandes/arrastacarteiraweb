package br.com.sandes.arrastacarteiraweb.interfaces;

import java.util.List;

import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.UsuarioDto;
import br.com.sandes.arrastacarteiraweb.model.Usuario;

public interface UsuarioService extends dao<Usuario, Long>{
	
	public Usuario localizarPorEmail(final String email);
	
	public UsuarioDto toDto(Usuario usuario);
	
	public Usuario fromDto(UsuarioDto usuarioDto);

	public List<Usuario> localizaPorCurso(CursoDto cursoDto);
	
	public Usuario salvar(Usuario usuario);
	

}
