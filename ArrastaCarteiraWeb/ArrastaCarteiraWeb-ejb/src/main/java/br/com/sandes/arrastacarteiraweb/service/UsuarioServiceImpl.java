package br.com.sandes.arrastacarteiraweb.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import br.com.sandes.arrastacarteiraweb.dao.UsuarioDao;
import br.com.sandes.arrastacarteiraweb.dto.CursoDto;
import br.com.sandes.arrastacarteiraweb.dto.UsuarioDto;
import br.com.sandes.arrastacarteiraweb.interfaces.CursoService;
import br.com.sandes.arrastacarteiraweb.interfaces.GrupoService;
import br.com.sandes.arrastacarteiraweb.interfaces.UsuarioService;
import br.com.sandes.arrastacarteiraweb.model.Grupo;
import br.com.sandes.arrastacarteiraweb.model.Usuario;

public class UsuarioServiceImpl implements UsuarioService {

	@Inject
	private UsuarioDao dao;

	@Inject
	private CursoService cursoService;

	@Inject
	private GrupoService grupoService;

	@Override
	public Usuario salvar(Usuario entity) {
		return dao.salvar(entity);
	}

	@Override
	public Usuario atualizar(Usuario entity) {
		return dao.atualizar(entity);
	}

	@Override
	public void remover(Long id) {
		dao.remover(id);

	}

	@Override
	public List<Usuario> getList() {
		return dao.getList();
	}

	@Override
	public Usuario encontrar(Long id) {
		return dao.encontrar(id);
	}

	@Override
	public Usuario localizarPorEmail(String email) {
		return dao.localizarPorEmail(email);
	}

	@Override
	public UsuarioDto toDto(Usuario usuario) {

		if (usuario == null) {
			return null;
		}
		final UsuarioDto dto = new UsuarioDto();

		dto.setId(usuario.getId());
		dto.setNome(usuario.getNome());
		dto.setEmail(usuario.getEmail());
		dto.setSenha(usuario.getSenha());
		
//		if (usuario.getListaGrupo() != null) {
//			dto.setListaGrupo(usuario.getListaGrupo().stream().map(entity -> this.grupoService.toDto(entity))
//					.collect(Collectors.toList()));
//		}
		
		dto.setCursoDto(cursoService.toDto(usuario.getCurso()));
		return dto;
	}

	@Override
	public Usuario fromDto(UsuarioDto usuarioDto) {
		final Usuario entity = new Usuario();

		if (usuarioDto.getCursoDto() != null) {
			entity.setCurso(cursoService.fromDto(usuarioDto.getCursoDto()));
		}
//		if (usuarioDto.getListaGrupo() != null) {
//			entity.setListaGrupo(usuarioDto.getListaGrupo().stream().map(dto -> this.grupoService.fromDto(dto))
//					.collect(Collectors.toList()));
//		}
		entity.setEmail(usuarioDto.getEmail());
		entity.setNome(usuarioDto.getNome());
		entity.setSenha(usuarioDto.getSenha());
		entity.setId(usuarioDto.getId());

		return entity;
	}
	
	

	@Override
	public List<Usuario> localizaPorCurso(CursoDto cursoDto) {
		return dao.localizarPorCurso(cursoService.fromDto(cursoDto));
	}

}
