package br.com.sandes.arrastacarteiraweb.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class UsuarioDto {

	private Long id;
	private String nome;
	private String email;
	private String senha;
	@JsonBackReference
	private List<GrupoDto> listaGrupo;
	private CursoDto cursoDto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public CursoDto getCursoDto() {
		return cursoDto;
	}

	public void setCursoDto(CursoDto cursoDto) {
		this.cursoDto = cursoDto;
	}

	public List<GrupoDto> getListaGrupo() {
		return listaGrupo;
	}

	public void setListaGrupo(List<GrupoDto> listaGrupo) {
		this.listaGrupo = listaGrupo;
	}

}
