package br.com.sandes.arrastacarteiraweb.interfaces;

import br.com.sandes.arrastacarteiraweb.dto.GrupoDto;
import br.com.sandes.arrastacarteiraweb.dto.TarefaDto;
import br.com.sandes.arrastacarteiraweb.model.Grupo;
import br.com.sandes.arrastacarteiraweb.model.Tarefa;

public interface TarefaService extends dao<Tarefa, Integer> {

	Tarefa fromDto(TarefaDto tarefaDto);
	
	Tarefa fromDto(TarefaDto tarefaDto, Grupo grupo);

	TarefaDto toDto(Tarefa tarefa);
	TarefaDto toDto(Tarefa tarefa, GrupoDto grupoDto);

}
