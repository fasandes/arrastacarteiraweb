package br.com.sandes.arrastacarteiraweb.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class GrupoDto {

	private Integer id;
	private String nome;
	private CursoDto cursoDto;
	private Boolean privado;
	private List<UsuarioDto> listaUsuarios;

	private List<TarefaDto> listaTarefas;

	public GrupoDto() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public CursoDto getCursoDto() {
		return cursoDto;
	}

	public void setCursoDto(CursoDto cursoDto) {
		this.cursoDto = cursoDto;
	}

	public Boolean getPrivado() {
		return privado;
	}

	public void setPrivado(Boolean privado) {
		this.privado = privado;
	}

	public List<UsuarioDto> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioDto> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<TarefaDto> getListaTarefas() {
		return listaTarefas;
	}

	public void setListaTarefas(List<TarefaDto> listaTarefas) {
		this.listaTarefas = listaTarefas;
	}

}
