package br.com.sandes.arrastacarteiraweb.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.sandes.arrastacarteiraweb.dao.TarefaDao;
import br.com.sandes.arrastacarteiraweb.dto.GrupoDto;
import br.com.sandes.arrastacarteiraweb.dto.TarefaDto;
import br.com.sandes.arrastacarteiraweb.interfaces.GrupoService;
import br.com.sandes.arrastacarteiraweb.interfaces.TarefaService;
import br.com.sandes.arrastacarteiraweb.model.Grupo;
import br.com.sandes.arrastacarteiraweb.model.Tarefa;

@Stateless
public class TarefaServiceImpl implements TarefaService {

	@Inject
	private TarefaDao dao;

	@Inject
	private GrupoService grupoService;

	@Override
	public Tarefa salvar(Tarefa entity) {
		return dao.salvar(entity);
	}

	@Override
	public Tarefa atualizar(Tarefa entity) {
		return dao.atualizar(entity);
	}

	@Override
	public void remover(Integer id) {
		dao.remover(id);

	}

	@Override
	public List<Tarefa> getList() {
		return dao.getList();
	}

	@Override
	public Tarefa encontrar(Integer id) {
		return dao.encontrar(id);
	}

	@Override
	public TarefaDto toDto(Tarefa tarefa) {

		if (tarefa == null) {
			return null;
		}
		final TarefaDto dto = new TarefaDto();

		dto.setId(tarefa.getId());
		dto.setNome(tarefa.getNome());
		dto.setRealizada(tarefa.getRealizada());
		
		if (tarefa.getGrupo() != null) {
			dto.setGrupoDto(grupoService.toDto(tarefa.getGrupo()));
		}

		return dto;
	}

	@Override
	public Tarefa fromDto(TarefaDto tarefaDto) {
		final Tarefa entity = new Tarefa();

		entity.setId(tarefaDto.getId());
		entity.setNome(tarefaDto.getNome());
		entity.setRealizada(tarefaDto.getRealizada());
		if (tarefaDto.getGrupoDto() != null) {
			entity.setGrupo(grupoService.fromDto(tarefaDto.getGrupoDto()));
		}
		return entity;
	}

	@Override
	public Tarefa fromDto(TarefaDto tarefaDto, Grupo grupo) {
		final Tarefa entity = new Tarefa();

		entity.setId(tarefaDto.getId());
		entity.setNome(tarefaDto.getNome());
		entity.setRealizada(tarefaDto.getRealizada());
		entity.setGrupo(grupo);
		
		return entity;
	}

	@Override
	public TarefaDto toDto(Tarefa tarefa, GrupoDto grupoDto) {
		if (tarefa == null) {
			return null;
		}
		final TarefaDto dto = new TarefaDto();

		dto.setId(tarefa.getId());
		dto.setNome(tarefa.getNome());
		dto.setRealizada(tarefa.getRealizada());
		
		dto.setGrupoDto(grupoDto);

		return dto;
	}

}
