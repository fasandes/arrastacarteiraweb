package br.com.sandes.arrastacarteiraweb.dao;

import br.com.sandes.arrastacarteiraweb.model.Instituicao;

public class InstituicaoDao extends GenericDao<Instituicao, Integer> {

	public InstituicaoDao() {
		super(Instituicao.class);
	}

}
