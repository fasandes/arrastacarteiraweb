package br.com.sandes.arrastacarteiraweb.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Usuario.class)
public class Usuario_ {
	
	public static volatile SingularAttribute<Usuario,Long> id;
	public static volatile SingularAttribute<Usuario,String> nome;
	public static volatile SingularAttribute<Usuario,String> email;
	public static volatile SingularAttribute<Usuario,String> senha;
	public static volatile SingularAttribute<Usuario,Curso> curso;


	

}
